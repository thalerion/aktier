var http = require('http');
var feed = require("feed-read");
var urls = ["http://gustavsaktieblogg.blogspot.com/feeds/posts/default",
"http://cristofferstockman.blogspot.com/feeds/posts/default"];
    
http.createServer(function (req, res) {
    res.writeHead(200, {'Content-Type': 'text/plain'});
    
     // setup simple html page:
    res.write("<html>\n<head>\n<title>RSS Feeds</title>\n</head>\n<body>");

     res.write("Test");
    // loop through our list of RSS feed urls
    for (var j = 0; j < urls.length; j++) {

        // fetch rss feed for the url:
        feed(urls[j], function(err, articles) {

            // loop through the list of articles returned
            for (var i = 0; i < articles.length; i++) {

                // stream article title (and what ever else you want) to client
                res.write("<h3>"+articles[i].title +"</h3>"); 

                // check we have reached the end of our list of articles & urls
                if( i === articles.length-1 && j === urls.length-1) {
                    res.end("</body>\n</html>"); // end http response
                } // else still have rss urls to check
            } //  end inner for loop
        }); // end call to feed (feed-read) method
    } // end urls for loop
    
}).listen(process.env.PORT, process.env.IP);